#tentative d'utiliser pymunk, mais bug que l'on ne comprend pas

import sys
import pymunk
import pygame
from pygame.locals import *


def main():
    pygame.init()
    screen = pygame.display.set_mode((600, 600))
    pygame.display.set_caption("Joints. Just wait and the L will tip over")
    clock = pygame.time.Clock()
    espace = pymunk.space.Space()
    espace.gravity = (0.0, -900.0)

    while True:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                sys.exit(0)

        screen.fill((255, 255, 255))

        espace.step(1/50.0)

        pygame.display.flip()
        clock.tick(50)


sys.exit(main())
