import matplotlib.pyplot as plt
import numpy as np
from generate_universe import *

#Fonction qui prend en paramètre un univers et qui affiche cet univers
def afficher_univers(universe):
    dim_universe=universe.shape
    image=plt.imshow(universe,cmap="Greys",interpolation="none")
    plt.axis('off')
    plt.show()

if __name__ == '__main__':
    univers=generate_universe((20,20))
    seed=create_seed("die_hard")
    univers_complet=add_seed_to_universe(seed,univers)
    afficher_univers(univers_complet)
