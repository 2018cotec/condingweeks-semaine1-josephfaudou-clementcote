import numpy as np
import random as rd

#Fonction qui prend en paramètre la taille de l'univers et qui renvoie l'univers de la taille correspondante
def generate_universe(size):
    universe = np.zeros(size)
    return universe

#Fonction qui prend en paramètre le nom de la seed et qui renvoie la seed en question
def create_seed(type_seed):
    seeds = {"boat": np.array([[1, 1, 0],
                               [1, 0, 1],
                               [0, 1, 0]]),
             "r_pentomino": np.array([[0, 1, 1],
                                      [1, 1, 0],
                                      [0, 1, 0]]),
             "beacon": np.array([[1, 1, 0, 0],
                                 [1, 1, 0, 0],
                                 [0, 0, 1, 1],
                                 [0, 0, 1, 1]]),
             "acorn": np.array([[0, 1, 0, 0, 0, 0, 0],
                                [0, 0, 0, 1, 0, 0, 0],
                                [1, 1, 0, 0, 1, 1, 1]]),
             "block_switch_engine": np.array([[0, 0, 0, 0, 0, 0, 1, 0],
                                              [0, 0, 0, 0, 1, 0, 1, 1],
                                              [0, 0, 0, 0, 1, 0, 1, 0],
                                              [0, 0, 0, 0, 1, 0, 0, 0],
                                              [0, 0, 1, 0, 0, 0, 0, 0],
                                              [1, 0, 1, 0, 0, 0, 0, 0]]),
             "infinite": np.array([[1, 1, 1, 0, 1],
                                   [1, 0, 0, 0, 0],
                                   [0, 0, 0, 1, 1],
                                   [0, 1, 1, 0, 1],
                                   [1, 0, 1, 0, 1]]),
             "die_hard": np.array([[0,0,0,0,0,0,1,0],
                                   [1,1,0,0,0,0,0,0],
                                   [0,1,0,0,0,1,1,1]]),
             "ten": np.array([[1,1,1,1,1,1,1,1,1,1]]),
             "gosper_glider_gun": np.array([[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                                            [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                                            [1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                                            [1,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]])}

    return seeds[type_seed]

#Fonction qui prend en paramètre une seed, un univers, et la position souhaitée de la seed de manière facultative, et qui renvoie l'univers muni de la seed, aux coordonnées fournies u aléatoirement
def add_seed_to_universe(seed, universe, x_start=-1, y_start=-1):
    dim_universe=universe.shape
    dim_seed=seed.shape
    if (dim_seed[0]>dim_universe[0]) or (dim_seed[1]> dim_universe[1]):
        print("Univers trop petit")
        return universe
    if x_start == -1:
        x_start=rd.randint(0,dim_universe[0]-dim_seed[0])
    if y_start == -1:
        y_start=rd.randint(0,dim_universe[1]-dim_seed[1])

    for line in range(dim_seed[0]):
        for column in range(dim_seed[1]):
            universe[x_start+line,y_start+column]=seed[line,column]
    return universe

#Fonction qui crée un univers avec une seed donnée
def generate_complete_universe(size,seed,x_start=-1,y_start=-1):
    seed = create_seed(type_seed = seed)
    universe = generate_universe(size)
    universe = add_seed_to_universe(seed, universe,x_start, y_start)
    return universe

