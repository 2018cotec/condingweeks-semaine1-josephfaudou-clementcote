from game_of_life_rules import *
from generate_universe import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib as mpl

#Configuration de matplotlib pour ImageMagick
mpl.rcParams['animation.ffmpeg_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/ffmpeg.exe"
mpl.rcParams['animation.convert_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/magick.exe"

#Fonction qui anime un univers avec la seed choisie
def animer_univers(size=(6,6),seed_name='beacon',speed=500,n=30,saved=False,cmap_choice="Greys",x_start=-1,y_start=-1):
    global universe
    if saved:
        mpl.use('Agg')
        universe=generate_complete_universe(size,seed_name,x_start,y_start) #Génération de l'unvivers
        fig=plt.figure()
        im=plt.imshow(universe,cmap="Greys",interpolation="none", animated=True) #Affiche l'univers au départ


        def update_image(*args): #Fonction de mise à jour de l'animation
            global universe
            universe=generation(universe)
            im.set_array(universe)
            return im,

        ani=animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
        plt.axis('off')
        ani.save('beacon.gif', dpi=80, writer='imagemagick', fps=1000/speed) #sauvegarde de l'animation
    else:
        universe=generate_complete_universe(size,seed_name,x_start,y_start) #Génération de l'unvivers
        fig=plt.figure()
        im=plt.imshow(universe,cmap=cmap_choice,interpolation="none", animated=True) #Affiche l'univers au départ


        def update_image(*args): #Fonction de mise à jour de l'animation
            global universe
            universe=generation(universe)
            im.set_array(universe)
            return im,

        ani=animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
        plt.axis('off')
        plt.show() #affichage de l'animation

if __name__=='__main__':
    animer_univers(size=(75,75),seed_name='block_switch_engine',speed=100,x_start=37,y_start=37)
