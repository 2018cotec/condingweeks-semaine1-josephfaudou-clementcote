import requests

def download(url,filename):
    r = requests.get(url)
    with open(filename, 'wb') as f:
        f.write(r.content)

if __name__=='__main__':
    download('http://pentadecathlon.com/lifeNews/2011/08/2011-08-26-c7-extensible.rle?text','c7-extensible.rle')
