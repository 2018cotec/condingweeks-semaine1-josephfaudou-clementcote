"""
b mort
o vivant
$ fin de ligne
bo$ donne bob (par défaut, compléter la ligne avec des b)
! fin du fichier
"""

import numpy as np

#Decodage des RLE
values="bo"

def decode_rle(file):
    #Ouverture et récupération des données du fichier
    with open(file,'r') as f:
        file_content=f.readlines()

    #Suppression des \n à la fin de cgque ligne
    for y in range(len(file_content)):
        file_content[y]=file_content[y].replace('\n','')
        file_content[y]=file_content[y].replace('!','')

    #Traitement de la première ligne
    first_line=file_content[0].split(', ')
    y_dim=int(first_line[0].split(' ')[2])
    x_dim=int(first_line[1].split(' ')[2])

    #Création de l'array correspondant
    seed=np.zeros((x_dim,y_dim),dtype=int)

    #Parcours des données
    content=''
    for y in range(len(file_content)-1):
        content+=file_content[y+1]

    #liste contenant chaque ligne encodée
    content=content.split('$')

    for y in range(len(content)):   #on traite les lignes une par une
        line = content[y]
        x=0    #abscisse dans le seed
        l=0
        while l<len(line): #on parcourt la ligne
            k=1    #le k permettra de se placer au prochain groupement de 0 ou de 1
            if line[l] in values: #cas où il n'y a qu'un seul 0 ou 1
                nb=1    #nb : nombre de 0 ou de 1 à la suite
            else:       #cas où il y a plusieurs 0 ou 1 à la suite
                while l+k<len(line) and line[l+k] not in values:
                    k+=1
                nb=int(line[l:l+k])
                l=l+k
            if l<len(line):
                if line[l]=='o':  #si la cellule est vivante, transforme en 1
                    for count in range(nb):
                        seed[y,x+count]=1
                    x=x+nb
                else:
                    x=x+nb
            l+=1
    return seed
