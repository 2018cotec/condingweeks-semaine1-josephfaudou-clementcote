import numpy as np
import random as rd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib as mpl
import tkinter as tk
import tkinter.ttk as ttk
import pickle
import requests
import os

# Configuration de matplotlib pour ImageMagick
mpl.rcParams['animation.ffmpeg_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/ffmpeg.exe"
mpl.rcParams['animation.convert_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/magick.exe"

# Récupération des seeds depuis le fichier
with open('seeds.dictionnary', 'rb') as file:
    save = pickle.Unpickler(file)
    seeds = save.load()

# Liste des colormaps
colormaps = ['viridis', 'plasma', 'inferno', 'magma', 'Greys', 'Purples', 'Blues', 'Greens', 'Oranges', 'Reds',
             'spring', 'cool', 'winter', 'autumn']


# Fonction qui prend en paramètre la taille de l'univers et qui renvoie l'univers de la taille correspondante
def generate_universe(size):
    universe = np.zeros(size)
    return universe


# Fonction qui prend en paramètre le nom de la seed et qui renvoie la seed en question
def create_seed(type_seed):
    return seeds[type_seed]


# Fonction qui prend en paramètre une seed, un univers, et la position souhaitée de la seed de manière facultative, et qui renvoie l'univers muni de la seed, aux coordonnées fournies u aléatoirement
def add_seed_to_universe(seed, universe, x_start=-1, y_start=-1):
    dim_universe = universe.shape
    dim_seed = seed.shape
    if (dim_seed[0] > dim_universe[0]) or (dim_seed[1] > dim_universe[1]):
        print("Univers trop petit")
        return universe
    if x_start == -1:
        x_start = rd.randint(0, dim_universe[0] - dim_seed[0])
    if y_start == -1:
        y_start = rd.randint(0, dim_universe[1] - dim_seed[1])

    for line in range(dim_seed[0]):
        for column in range(dim_seed[1]):
            universe[x_start + line, y_start + column] = seed[line, column]
    return universe


# Fonction qui crée un univers avec une seed donnée
def generate_complete_universe(size, seed, x_start=-1, y_start=-1):
    seed = create_seed(type_seed=seed)
    universe = generate_universe(size)
    universe = add_seed_to_universe(seed, universe, x_start, y_start)
    return universe


def survival(univers, x,
             y):  # fonction qui prend en arguments l'univers et une cellule et retourne true si elle change de statut, false sinon
    dim_univers = univers.shape
    voisins_vivants = 0
    voisins = [[x - 1, y + 1], [x, y + 1], [x + 1, y + 1], [x + 1, y], [x + 1, y - 1], [x, y - 1], [x - 1, y - 1],
               [x - 1, y]]
    for voisin in voisins:  # on parcourt les voisins pour vérifier s'ils sont dans l'univers, et sinon on prend en compte le fait que l'univers est un torre
        if voisin[0] == dim_univers[0]:
            voisin[0] = 0
        if voisin[1] == dim_univers[1]:
            voisin[1] = 0
        if univers[voisin[0], voisin[1]] == 1:
            voisins_vivants += 1
    if univers[x][y] == 0:  # si une cellule est morte, on regarde si elle devient vivante
        if voisins_vivants == 3:
            return True
    if univers[x][y] == 1:  # si une cellule est en vie, on regarde si elle meurt
        if not (voisins_vivants == 2 or voisins_vivants == 3):
            return True
    return False


# Fonction qui prend un univers et qui renvoie un array de la taille de l'univers disant si une cellule doit changer d'état ou pas
def universe_change(universe):
    dim_universe = universe.shape
    changes = np.zeros(dim_universe, dtype=bool)
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            changes[line, column] = survival(universe, line, column)
    return changes


# Fonction qui prend un univers en argument et renvoie l'univers à l'itération suivante
def generation(universe):
    changes = universe_change(universe)
    dim_universe = universe.shape
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            if changes[line, column]:
                universe[line, column] = 1 - universe[line, column]
    return universe


# Fonction qui prends en argument la taille de l'univers, le seed, le nombre d'itérations et la position du seed dans l'univers(facultatif) et renvoi une liste d'univers pour chaque itération
def game_life_simulate(size, seed, n, x_lim=-1, y_lim=-1):
    univers = generate_universe(size)
    seed = create_seed(seed)
    univers_complet = add_seed_to_universe(seed, univers, x_lim, y_lim)
    liste_univers = [univers_complet]
    for iteration in range(n):
        univers_complet = generation(univers_complet)
        liste_univers.append(univers_complet)
    return liste_univers


# Fonction qui anime un univers avec la seed choisie
def animer_univers(size=(6, 6), seed_name='beacon', speed=500, n=30, saved=False, cmap_choice="Greys", x_start=-1,
                   y_start=-1):
    global universe
    if n == -1:
        n = None
    if saved:
        mpl.use('Agg')
        universe = generate_complete_universe(size, seed_name, x_start, y_start)  # Génération de l'unvivers
        fig = plt.figure()
        im = plt.imshow(universe, cmap=cmap_choice, interpolation="none", animated=True)  # Affiche l'univers au départ

        def update_image(*args):  # Fonction de mise à jour de l'animation
            global universe
            universe = generation(universe)
            im.set_array(universe)
            return im,

        ani = animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n, repeat=False)  # Animation
        plt.axis('off')
        ani.save(seed_name + '.gif', dpi=80, writer='imagemagick', fps=1000 / speed)  # sauvegarde de l'animation
        tk.messagebox.showinfo("Terminé", "Le gif a été créé avec succès !")
    else:
        universe = generate_complete_universe(size, seed_name, x_start, y_start)  # Génération de l'unvivers
        fig = plt.figure()
        im = plt.imshow(universe, cmap=cmap_choice, interpolation="none", animated=True)  # Affiche l'univers au départ

        def update_image(*args):  # Fonction de mise à jour de l'animation
            global universe
            universe = generation(universe)
            im.set_array(universe)
            return im,

        ani = animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n, repeat=False)  # Animation
        plt.axis('off')
        plt.show()  # affichage de l'animation


# Decodage des RLE
values = "bo"


def decode_rle(file):
    # Ouverture et récupération des données du fichier
    with open(file, 'r') as f:
        file_content = f.readlines()

    # Suppression des commentaires en début de fichier
    to_suppr = []
    for y in range(len(file_content)):
        if file_content[y][0] == "#":
            to_suppr.append(y)
    for i in range(len(to_suppr) - 1, -1, -1):
        file_content.pop(to_suppr[i])

    # Suppression des \n à la fin de chaque ligne
    for y in range(len(file_content)):
        file_content[y] = file_content[y].replace('\n', '')
        file_content[y] = file_content[y].replace('!', '')

    # Traitement de la première ligne
    first_line = file_content[0].split(', ')
    y_dim = int(first_line[0].split(' ')[2])
    x_dim = int(first_line[1].split(' ')[2])

    # Création de l'array correspondant
    seed = np.zeros((x_dim, y_dim), dtype=int)

    # Parcours des données
    content = ''
    for y in range(len(file_content) - 1):
        content += file_content[y + 1]

    # liste contenant chaque ligne encodée
    content = content.split('$')

    for y in range(len(content)):  # on traite les lignes une par une
        line = content[y]
        x = 0
        l = 0
        while l < len(line):  # on parcourt la ligne
            k = 1  # le k permettra de se placer au prochain groupement de 0 ou de 1
            if line[l] in values:  # cas où il n'y a qu'un seul 0 ou 1
                nb = 1  # nb : nombre de 0 ou de 1 à la suite
            else:  # cas où il y a plusieurs 0 ou 1 à la suite
                while l + k < len(line) and line[l + k] not in values:
                    k += 1
                nb = int(line[l:l + k])
                l = l + k
            if l < len(line):
                if line[l] == 'o':  # si la cellule est vivante, transforme en 1
                    for count in range(nb):
                        seed[y, x + count] = 1
                    x = x + nb
                else:
                    x = x + nb
            l += 1
    return seed


# Fonction qui télécharge un fichier depuis url vers filename
def download(url, filename):
    r = requests.get(url)
    with open(filename, 'wb') as f:
        f.write(r.content)


# Fonction qui ajoute une seed au dictionnaire
def add_seed(seeds_dict, url, filename):
    download(url, filename + ".rle")
    seed = decode_rle(filename + '.rle')
    if filename not in seeds_dict.keys():
        seeds_dict[filename] = seed
    os.remove(filename + ".rle")


if __name__ == "__main__":

    # Fonction qui récupère les valeurs entrées et lance la simulation
    def lancer_game_of_life():
        sizex = int(sizex_text.get())
        sizey = int(sizey_text.get())
        cmap = cmap_list.get()
        seed = seed_list.get()
        speed = int(speed_text.get())
        iterations = int(n_text.get())
        save = save_checked.get()
        x_start = int(x_start_text.get())
        y_start = int(y_start_text.get())
        animer_univers((sizex, sizey), seed, speed, iterations, save, cmap, x_start, y_start)


    def add_seed_prompt():
        global window
        global url_prompt_text
        global filename_prompt_text
        global prompt
        prompt = tk.Toplevel(window, bg='white')
        prompt.resizable(False, False)
        prompt.title("Ajout d'une seed")
        prompt.iconbitmap('icone.ico')

        title_prompt = tk.Label(prompt, text="Ajout d'une seed", font=("Calibri", 14, 'bold'), bg='white')

        frame_prompt = tk.Frame(prompt, bg='white')

        url_prompt_label = tk.Label(frame_prompt, text="URL du fichier", font=("Calibri", 11), bg='white')
        filename_prompt_label = tk.Label(frame_prompt, text="Nom du fichier", font=("Calibri", 11), bg='white')
        url_prompt_text = tk.StringVar(frame_prompt)
        filename_prompt_text = tk.StringVar(frame_prompt)
        url_prompt_entry = tk.Entry(frame_prompt, textvariable=url_prompt_text, font=("Calibri", 11), bg='white')
        filename_prompt_entry = tk.Entry(frame_prompt, textvariable=filename_prompt_text, font=("Calibri", 11),
                                         bg='white')
        url_prompt_label.grid(row=0, column=0)
        filename_prompt_label.grid(row=1, column=0)
        url_prompt_entry.grid(row=0, column=1)
        filename_prompt_entry.grid(row=1, column=1)

        button_prompt = tk.Button(prompt, text="Ajouter seed", activebackground="white", fg="black",
                                  command=download_seed, font=("Calibri", 12), bg='white', relief='groove')

        title_prompt.grid(row=0, column=0)
        frame_prompt.grid(row=1, column=0)
        button_prompt.grid(row=2, column=0)


    def download_seed():
        global url_prompt_text
        global filename_prompt_text
        global seeds
        global seed_list
        global prompt
        url = url_prompt_text.get()
        filename = filename_prompt_text.get()
        if filename not in seeds.keys():
            add_seed(seeds, url, filename)
            tk.messagebox.showinfo("Ajout seed", "La seed a été ajoutée avec succès !")
        else:
            tk.messagebox.showerror("Ajout seed", "Cette seed existe déjà")
        seed_list.configure(values=tuple(seeds.keys()))
        prompt.destroy()


    window = tk.Tk()  # créer une fenêtre tkinter
    window.title("Le jeu de la vie")
    window.resizable(False, False)  # empêche de redimensionner la fenêtre
    window.iconbitmap('icone.ico')

    frame_global = tk.Frame(window, padx=10, pady=10, bg='white')

    bienvenue = tk.Label(frame_global, text="Bienvenue dans le jeu de la vie !", font=("Calibri", 30, 'bold'), pady=5,
                         bg='white')  # font modifie la police d'écriture
    bienvenue.grid(row=0, column=0)  # affiche un message de bienvenu en-haut de la fenêtre

    frame1 = tk.Frame(frame_global, pady=5, bg='white')
    frame_save = tk.Frame(frame1, bg='white')

    sizex_text = tk.StringVar(frame1)  # permet d'entrer la largeur de l'univers
    sizex_text.set("50")  # valeur par défaut
    sizex_label = tk.Label(frame1, text="Hauteur de l'univers", font=("Calibri", 14), bg='white')
    sizex_label.grid(row=0, column=0)
    sizex_entry = tk.Entry(frame1, textvariable=sizex_text, width=30, font=("Calibri", 14), bg='white')
    sizex_entry.grid(row=0, column=1)

    sizey_text = tk.StringVar(frame1)
    sizey_text.set("50")
    sizey_label = tk.Label(frame1, text="Largeur de l'univers", font=("Calibri", 14), bg='white')
    sizey_label.grid(row=1, column=0)
    sizey_entry = tk.Entry(frame1, textvariable=sizey_text, width=30, font=("Calibri", 14), bg='white')
    sizey_entry.grid(row=1, column=1)

    # ajoute menu déroulant pour choisir le seed
    seed_label = tk.Label(frame1, text="Seed de départ", font=("Calibri", 14), bg='white')
    seed_label.grid(row=8, column=0)
    seed_list = ttk.Combobox(frame1, values=tuple(seeds.keys()), width=28, font=("Calibri", 14), background='white')
    seed_list.current(2)
    seed_list.grid(row=8, column=1)

    # bouton pour ajouter un seed
    seed_button = tk.Button(frame1, text="Ajouter un seed", activebackground="white", fg="black",
                            command=add_seed_prompt, font=("Calibri", 13), bg='white', relief='groove')
    seed_button.grid(row=9, column=1)

    cmap_label = tk.Label(frame1, text="Choix de la cmap", font=("Calibri", 14), bg='white')
    cmap_label.grid(row=2, column=0)
    cmap_list = ttk.Combobox(frame1, values=colormaps, width=28, font=("Calibri", 14), background='white')
    cmap_list.current(4)
    cmap_list.grid(row=2, column=1)

    speed_text = tk.StringVar(frame1)
    speed_text.set("300")
    speed_label = tk.Label(frame1, text="Temps en millisecondes entre deux images", font=("Calibri", 14), bg='white')
    speed_label.grid(row=3, column=0)
    speed_entry = tk.Entry(frame1, textvariable=speed_text, width=30, font=("Calibri", 14), bg='white')
    speed_entry.grid(row=3, column=1)

    n_text = tk.StringVar(frame1)
    n_text.set("30")
    n_label = tk.Label(frame1, text="Nombre d'itérations (-1 pour une infinité)", font=("Calibri", 14), bg='white')
    n_label.grid(row=7, column=0)
    n_entry = tk.Entry(frame1, textvariable=n_text, width=30, font=("Calibri", 14), bg='white')
    n_entry.grid(row=7, column=1)

    save_checked = tk.BooleanVar(frame_save,
                                 "0")  # permet de cocher une case si on veut sauvegarder la simulation. Par défaut pas cochée.
    save_label = tk.Label(frame_save, text="Sauvegarder la simulation ?", font=("Calibri", 14), bg='white')
    save_label.grid(row=0, column=0)
    save_checkbox = tk.Checkbutton(frame_save, variable=save_checked, font=("Calibri", 14), bg='white',
                                   activebackground='white')
    save_checkbox.grid(row=0, column=1)

    x_start_text = tk.StringVar(frame1)
    x_start_text.set("-1")
    x_start_label = tk.Label(frame1, text="Ordonnée de départ du seed (-1 pour aléatoire)", font=("Calibri", 14),
                             bg='white')
    x_start_label.grid(row=6, column=0)
    x_start_entry = tk.Entry(frame1, textvariable=x_start_text, width=30, font=("Calibri", 14), bg='white')
    x_start_entry.grid(row=6, column=1)

    y_start_text = tk.StringVar(frame1)
    y_start_text.set("-1")
    y_start_label = tk.Label(frame1, text="Abscisse de départ du seed (-1 pour aléatoire)", font=("Calibri", 14),
                             bg='white')
    y_start_label.grid(row=5, column=0)
    y_start_entry = tk.Entry(frame1, textvariable=y_start_text, width=30, font=("Calibri", 14), bg='white')
    y_start_entry.grid(row=5, column=1)

    frame_save.grid(row=9, column=0)
    frame1.grid(row=1, column=0)  # ajoute le tableau avec les valeurs dans la fenêtre principale

    button = tk.Button(frame_global, text="Lancer simulation", activebackground="white", fg="black",
                       command=lancer_game_of_life, font=("Calibri", 14), bg='white',
                       relief='groove')  # bouton pour activer la fonction qui active la simulation
    button.grid(row=2, column=0)

    frame_global.grid()

    window.mainloop()  # ouvre la fenêtre
    # Sauvegarde de la seed dans le fichier
    with open('seeds.dictionnary', 'wb') as file:
        save = pickle.Pickler(file)
        save.dump(seeds)
