import numpy as np
from generate_universe import *

def survival(univers,x,y):        #fonction qui prend en arguments l'univers et une cellule et retourne true si elle change de statut, false sinon
    dim_univers=univers.shape
    voisins_vivants=0
    voisins=[[x-1,y+1],[x,y+1],[x+1,y+1],[x+1,y],[x+1,y-1],[x,y-1],[x-1,y-1],[x-1,y]]
    for voisin in voisins:          #on parcourt les voisins pour vérifier s'ils sont dans l'univers, et sinon on prend en compte le fait que l'univers est un torre
        if voisin[0]==dim_univers[0]:
            voisin[0]=0
        if voisin[1]==dim_univers[1]:
            voisin[1]=0
        if univers[voisin[0],voisin[1]]==1:
            voisins_vivants+=1
    if univers[x][y]==0:            #si une cellule est morte, on regarde si elle devient vivante
        if voisins_vivants==3:
            return True
    if univers[x][y]==1:            #si une cellule est en vie, on regarde si elle meurt
        if not(voisins_vivants==2 or voisins_vivants==3):
            return True
    return False

#Fonction qui prend un univers et qui renvoie un array de la taille de l'univers disant si une cellule doit changer d'état ou pas
def universe_change(universe):
    dim_universe=universe.shape
    changes=np.zeros(dim_universe,dtype=bool)
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            changes[line,column]=survival(universe,line,column)
    return changes

#Fonction qui prend un univers en argument et renvoie l'univers à l'itération suivante
def generation(universe):
    changes=universe_change(universe)
    dim_universe=universe.shape
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            if changes[line,column]:
                universe[line,column]=1-universe[line,column]
    return universe

#Fonction qui prends en argument la taille de l'univers, le seed, le nombre d'itérations et la position du seed dans l'univers(facultatif) et renvoi une liste d'univers pour chaque itération
def game_life_simulate(size,seed,n,x_start=-1,y_start=-1):
    univers=generate_universe(size)
    seed=create_seed(seed)
    univers_complet=add_seed_to_universe(seed,univers, x_start,y_start)
    liste_univers=[univers_complet]
    for iteration in range (n):
        univers_complet=generation(univers_complet)
        liste_univers.append(univers_complet)
    return liste_univers


