from game_of_life_rules import *
from generate_universe import *
import numpy as np

def test_change():
    univers=generate_universe((3,3))
    seed=create_seed("boat")
    univers_complet=add_seed_to_universe(seed,univers,x_start=0,y_start=0)
    assert not survival(univers_complet,1,1)

def test_universe_change():
    univers=np.array([[0,0,0],
                     [0,1,0],
                     [0,0,0]])
    assert universe_change(univers).any()==np.array([[False,False,False],
                                                    [False,True,False],
                                                    [False,False,False]]).any()

def test_generation():
    univers=np.array([[0,0,0],
                     [0,1,0],
                     [0,0,0]])
    assert generation(univers).any()==np.array([[0,0,0],
                                                        [0,0,0],
                                                        [0,0,0]]).any()

def test_game_life_simulate():
    assert np.array(game_life_simulate((5,5),"boat",2,x_start=1,y_start=1)).any()==np.array([np.array([[0., 0., 0., 0., 0.],
                                                                                                       [0., 1., 1., 0., 0.],
                                                                                                       [0., 1., 0., 1., 0.],
                                                                                                       [0., 0., 1., 0., 0.],
                                                                                                       [0., 0., 0., 0., 0.]]),
                                                                                             np.array([[0., 0., 0., 0., 0.],
                                                                                                       [0., 1., 1., 0., 0.],
                                                                                                       [0., 1., 0., 1., 0.],
                                                                                                       [0., 0., 1., 0., 0.],
                                                                                                       [0., 0., 0., 0., 0.]]),
                                                                                             np.array([[0., 0., 0., 0., 0.],
                                                                                                       [0., 1., 1., 0., 0.],
                                                                                                       [0., 1., 0., 1., 0.],
                                                                                                       [0., 0., 1., 0., 0.],
                                                                                                       [0., 0., 0., 0., 0.]])]).any()

if __name__=='__main__':
    test_change()
    test_universe_change()
    test_generation()
    test_game_life_simulate()
