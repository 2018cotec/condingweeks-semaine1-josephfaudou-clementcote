"""
Abandonné car l'éxécution successive des fonctions à l'intérieur d'une fonction ralentissait beaucoup l'execution
Utilisation de matplotlib à la place
"""


from game_of_life_main import *
import tkinter as tk

def next_step():
    global universe
    global window2
    global dessin
    universe=generation(universe)
    dessin.delete() #supprime tout ce que contient le canvas
    #actualisation du canvas pour l'étape d'après
    for i in range(15):
        for j in range(15):
            if universe[j,i]==1:
                dessin.create_rectangle(str(i*10)+'m',str(j*10)+'m',str((i+1)*10)+'m',str((j+1)*10)+'m',fill='black',width=0)
            else:
                dessin.create_rectangle(str(i*10)+'m',str(j*10)+'m',str((i+1)*10)+'m',str((j+1)*10)+'m',fill='white',width=0)
    window2.after(10,next_step)#appelle la fonction next_step après 10ms

def commencer():
    global universe
    global window2
    global dessin
    #Crée un toplevel contenant un canvas
    window2=tk.Toplevel(window1)
    dessin=tk.Canvas(window2,background='white',heigh=str(15*10)+'m',width=str(15*10)+'m')
    dessin.grid()
    #initialisation du canvas
    for i in range(15):
        for j in range(15):
            if universe[j,i]==1:
                dessin.create_rectangle(str(i*10)+'m',str(j*10)+'m',str((i+1)*10)+'m',str((j+1)*10)+'m',fill='black',width=0)
            else:
                dessin.create_rectangle(str(i*10)+'m',str(j*10)+'m',str((i+1)*10)+'m',str((j+1)*10)+'m',fill='white',width=0)
    window2.after(10,next_step) #appelle la fonction next_step après 10ms

if __name__=='__main__':
    window1=tk.Tk()
    window1.resizable(False,False)
    button=tk.Button(window1,text='Commencer',command=commencer)
    button.grid()
    universe=generate_complete_universe((15,15),'Die_hard',1,1)
    window1.mainloop()
