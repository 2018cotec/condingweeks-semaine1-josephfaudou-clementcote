from animation import *
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("size", help="size of the universe",type=int)
parser.add_argument("seed_name", help="name of the seed")
parser.add_argument("speed", help="time in milliseconds between two images in the animation",type=int)
parser.add_argument("n", help="number of iterations, None for infinite",type=int)
parser.add_argument("saved", help="True will save a gif instead of showing the animation",type=bool)
parser.add_argument("cmap_choice", help="type of color")
parser.add_argument("x_start", help="x position for the seed",type=int)
parser.add_argument("y_start", help="y position for the seed",type=int)
args = parser.parse_args()

animer_univers((args.size,args.size),args.seed_name,args.speed,args.n,args.saved,args.cmap_choice,args.x_start,args.y_start)
