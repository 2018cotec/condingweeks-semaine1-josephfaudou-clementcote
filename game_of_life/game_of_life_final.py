import numpy as np
import random as rd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib as mpl

#Configuration de matplotlib pour ImageMagick
mpl.rcParams['animation.ffmpeg_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/ffmpeg.exe"
mpl.rcParams['animation.convert_path'] = "C:/Program Files/ImageMagick-7.0.8-Q16/magick.exe"


#Fonction qui prend en paramètre la taille de l'univers et qui renvoie l'univers de la taille correspondante
def generate_universe(size):
    universe = np.zeros(size)
    return universe

#Fonction qui prend en paramètre le nom de la seed et qui renvoie la seed en question
def create_seed(type_seed):
    seeds = {"boat": np.array([[1, 1, 0],
                               [1, 0, 1],
                               [0, 1, 0]]),
             "r_pentomino": np.array([[0, 1, 1],
                                      [1, 1, 0],
                                      [0, 1, 0]]),
             "beacon": np.array([[1, 1, 0, 0],
                                 [1, 1, 0, 0],
                                 [0, 0, 1, 1],
                                 [0, 0, 1, 1]]),
             "acorn": np.array([[0, 1, 0, 0, 0, 0, 0],
                                [0, 0, 0, 1, 0, 0, 0],
                                [1, 1, 0, 0, 1, 1, 1]]),
             "block_switch_engine": np.array([[0, 0, 0, 0, 0, 0, 1, 0],
                                              [0, 0, 0, 0, 1, 0, 1, 1],
                                              [0, 0, 0, 0, 1, 0, 1, 0],
                                              [0, 0, 0, 0, 1, 0, 0, 0],
                                              [0, 0, 1, 0, 0, 0, 0, 0],
                                              [1, 0, 1, 0, 0, 0, 0, 0]]),
             "infinite": np.array([[1, 1, 1, 0, 1],
                                   [1, 0, 0, 0, 0],
                                   [0, 0, 0, 1, 1],
                                   [0, 1, 1, 0, 1],
                                   [1, 0, 1, 0, 1]]),
             "die_hard": np.array([[0,0,0,0,0,0,1,0],
                                   [1,1,0,0,0,0,0,0],
                                   [0,1,0,0,0,1,1,1]]),
             "ten": np.array([[1,1,1,1,1,1,1,1,1,1]]),
             "gosper_glider_gun": np.array([[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                                            [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,1,1],
                                            [1,1,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                                            [1,1,0,0,0,0,0,0,0,0,1,0,0,0,1,0,1,1,0,0,0,0,1,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                                            [0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]])}

    return seeds[type_seed]

#Fonction qui prend en paramètre une seed, un univers, et la position souhaitée de la seed de manière facultative, et qui renvoie l'univers muni de la seed, aux coordonnées fournies u aléatoirement
def add_seed_to_universe(seed, universe, x_start=-1, y_start=-1):
    dim_universe=universe.shape
    dim_seed=seed.shape
    if (dim_seed[0]>dim_universe[0]) or (dim_seed[1]> dim_universe[1]):
        print("Univers trop petit")
        return universe
    if x_start == -1:
        x_start=rd.randint(0,dim_universe[0]-dim_seed[0])
    if y_start == -1:
        y_start=rd.randint(0,dim_universe[1]-dim_seed[1])

    for line in range(dim_seed[0]):
        for column in range(dim_seed[1]):
            universe[x_start+line,y_start+column]=seed[line,column]
    return universe

#Fonction qui crée un univers avec une seed donnée
def generate_complete_universe(size,seed,x_start=-1,y_start=-1):
    seed = create_seed(type_seed = seed)
    universe = generate_universe(size)
    universe = add_seed_to_universe(seed, universe,x_start, y_start)
    return universe

def survival(univers,x,y):        #fonction qui prend en arguments l'univers et une cellule et retourne true si elle change de statut, false sinon
    dim_univers=univers.shape
    voisins_vivants=0
    voisins=[[x-1,y+1],[x,y+1],[x+1,y+1],[x+1,y],[x+1,y-1],[x,y-1],[x-1,y-1],[x-1,y]]
    for voisin in voisins:          #on parcourt les voisins pour vérifier s'ils sont dans l'univers, et sinon on prend en compte le fait que l'univers est un torre
        if voisin[0]==dim_univers[0]:
            voisin[0]=0
        if voisin[1]==dim_univers[1]:
            voisin[1]=0
        if univers[voisin[0],voisin[1]]==1:
            voisins_vivants+=1
    if univers[x][y]==0:            #si une cellule est morte, on regarde si elle devient vivante
        if voisins_vivants==3:
            return True
    if univers[x][y]==1:            #si une cellule est en vie, on regarde si elle meurt
        if not(voisins_vivants==2 or voisins_vivants==3):
            return True
    return False

#Fonction qui prend un univers et qui renvoie un array de la taille de l'univers disant si une cellule doit changer d'état ou pas
def universe_change(universe):
    dim_universe=universe.shape
    changes=np.zeros(dim_universe,dtype=bool)
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            changes[line,column]=survival(universe,line,column)
    return changes

#Fonction qui prend un univers en argument et renvoie l'univers à l'itération suivante
def generation(universe):
    changes=universe_change(universe)
    dim_universe=universe.shape
    for line in range(dim_universe[0]):
        for column in range(dim_universe[1]):
            if changes[line,column]:
                universe[line,column]=1-universe[line,column]
    return universe

#Fonction qui prends en argument la taille de l'univers, le seed, le nombre d'itérations et la position du seed dans l'univers(facultatif) et renvoi une liste d'univers pour chaque itération
def game_life_simulate(size,seed,n,x_lim=-1,y_lim=-1):
    univers=generate_universe(size)
    seed=create_seed(seed)
    univers_complet=add_seed_to_universe(seed,univers, x_lim,y_lim)
    liste_univers=[univers_complet]
    for iteration in range (n):
        univers_complet=generation(univers_complet)
        liste_univers.append(univers_complet)
    return liste_univers

#Fonction qui anime un univers avec la seed choisie
def animer_univers(size=(6,6),seed_name='beacon',speed=500,n=30,saved=False,cmap_choice="Greys",x_start=-1,y_start=-1):
    global universe
    if saved:
        mpl.use('Agg')
        universe=generate_complete_universe(size,seed_name,x_start,y_start) #Génération de l'unvivers
        fig=plt.figure()
        im=plt.imshow(universe,cmap="Greys",interpolation="none", animated=True) #Affiche l'univers au départ


        def update_image(*args): #Fonction de mise à jour de l'animation
            global universe
            universe=generation(universe)
            im.set_array(universe)
            return im,

        ani=animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
        plt.axis('off')
        ani.save('beacon.gif', dpi=80, writer='imagemagick', fps=1000/speed) #sauvegarde de l'animation
    else:
        universe=generate_complete_universe(size,seed_name,x_start,y_start) #Génération de l'unvivers
        fig=plt.figure()
        im=plt.imshow(universe,cmap=cmap_choice,interpolation="none", animated=True) #Affiche l'univers au départ


        def update_image(*args): #Fonction de mise à jour de l'animation
            global universe
            universe=generation(universe)
            im.set_array(universe)
            return im,

        ani=animation.FuncAnimation(fig, update_image, interval=speed, blit=True, frames=n,repeat=False) #Animation
        plt.axis('off')
        plt.show() #affichage de l'animation

if __name__== "__main__":
    sizex=int(input("Entrer la largeur de l'univers (par défaut 50)") or "50")
    sizey=int(input("Entrer la hauteur de l'univers (par défaut 50)") or "50")
    seed=input("Entrer le nom du seed (par défaut beacon)") or "beacon"
    x_start=int(input("Entrer la position x du seed (par défaut aléatoire)") or "-1")
    y_start=int(input("Entrer la position y du seed (par défaut aléatoire)") or "-1")
    cmap_choice=input("Choisir la couleur (par défaut Greys)") or "Greys"
    speed=int(input("Entrer le temps entre deux itérations (par défaut 300 ms)") or "300")
    n=int(input("Entrer le nombre d'intération (par défaut infini)") or "-1")
    save=(input("Définir si on veut sauvegarder l'animation (par défaut False)") or "False")=="True"
    if n==-1:
        n=None
    animer_univers((sizex,sizey),seed,speed,n,save,cmap_choice,x_start,y_start)

