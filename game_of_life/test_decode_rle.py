from pytest import *
import numpy as np
from decode_rle import *

def test_decode_rle():
    assert decode_rle("glider.rle").any()==np.array([[0,1,0],[0,0,1],[1,1,1]]).any()

if __name__=="__main__":
    test_decode_rle()
